# Simulation for the Game of Life.

## Cell Pattern convention
The name of the cell pattern can be found online, 
e.g. to load Kok's galaxy:
https://www.conwaylife.com/wiki/Kok%27s\_galaxy
find the RLE page
https://www.conwaylife.com/patterns/koksgalaxy.rle
The name before .rle is the one we want. Not Kok%27s\_galaxy.
The construction of the farm is found in main.FarmFactory.java.

## Resources
https://www.conwaylife.com/wiki/

## To do:
- Convert set back to rle format.
- Timestepping farm.
- Enable animation and delete old code 'crate'.
