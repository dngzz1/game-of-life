package game_utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class TextFileHandler {
	/*
	 * General file reader.
	 */
	public static String readTextThrowingError(String fileName) throws IOException {
		StringBuilder sb = new StringBuilder();
		String everything = null;

		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line = br.readLine();

		while (line != null) {
			sb.append(line);
			sb.append(System.lineSeparator());
			line = br.readLine();
		}
		everything = sb.toString();
		br.close();
		return everything;
	}
	public static String readTextElseTerminate(String fileName)  {
		StringBuilder sb = new StringBuilder();
		String everything = null;

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fileName));
			String line = br.readLine();
			
			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			everything = sb.toString();
			br.close();
		} catch (IOException e) {
			System.out.println(fileName + " not found.");
			System.out.println("System terminating.");
			System.exit(0);
		}
		return everything;
	}
	
	/*
	 * General file writer.
	 */
	public static void writeText(String content, String fileName) {
		File file = new File(fileName);
		
		try (BufferedWriter br = new BufferedWriter(new FileWriter(file))) {
			br.write(content);
		} catch (IOException e) {
			System.out.println("Unable to write file " + file.toString());
			System.exit(0);
		}
	}
	
	/*
	 * General html scraper from the Internet.
	 * Does not throw exception, but terminates
	 * entire system if link is not found.
	 */
	public static String getHtmlFrom(String htmlLink) {
		StringBuilder sb = new StringBuilder();
		URL url;
		try {
			url = new URL(htmlLink);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			br.close();
		} catch (IOException e) {
			System.out.println(htmlLink + " not found.");
			System.out.println("Program terminated.");
			System.exit(0);
		}

//			System.out.println("HTML successfully retrieved from:");
//			System.out.println(htmlLink);
		return sb.toString();
	}
}
