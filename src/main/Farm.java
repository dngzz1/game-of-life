package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cell.Cell;
import cell.CellPattern;
import cell.Triple;

public class Farm {
	/*
	 * We are using a hash set to store cells, because cells are unordered and there
	 * are no duplicates.
	 */
	private int width;
	private int height;
	private Set<Cell> cellSet = new HashSet<>();
	private Map<Cell, Integer> neighbors;

	/*
	 * Constructor for Builder.
	 */
	public static class Builder {
		// Required parameters.
		private final int width;
		private final int height;

		// Data.
		private List<Triple<CellPattern, Integer, Integer>> data = new ArrayList<>();

		// Constructor.
		public Builder(int height, int width) {
			this.height = height;
			this.width = width;
		}

		public Builder add(String val, int i, int j) {
			Triple<CellPattern, Integer, Integer> triple = new Triple<>(new CellPattern(val), i, j);
			data.add(triple);
			return this;
		}

		public Farm build() {
			return new Farm(this);
		}
	}

	private Farm(Builder builder) {
		width = builder.width;
		height = builder.height;
		cellSet = initCells(builder.data);
		calculateNeighbors();
	}

	/*
	 * convert List<Triple<CellPattern, Integer, Integer>> to HashSet<Cell>.
	 */
	private Set<Cell> initCells(List<Triple<CellPattern, Integer, Integer>> data) {
		Set<Cell> cellSet = new HashSet<>();
		for (Triple<CellPattern, Integer, Integer> triple : data) {
			CellPattern cellPattern = triple.getFirst();
			int x = triple.getSecond();
			int y = triple.getThird();
			for (Cell cell : cellPattern.getCellSet()) {
				int newI = cell.getX() + x;
				int newJ = cell.getY() + y;
				cellSet.add(new Cell(newI, newJ));
			}
		}
		return cellSet;
	}

	/*
	 * Iterate one step.
	 */
	public void step() {
		Set<Cell> newCellSet = new HashSet<Cell>();
		// If N = 3, set alive.
		// If alive and N = 2, set alive.
		for (Cell cell : neighbors.keySet()) {
			int i = cell.getX();
			int j = cell.getY();
			if (i < 0 || i >= height || j < 0 || j >= width) {
				continue;
			}
			Cell newCell = new Cell(i, j);
			int N = neighbors.get(newCell);
			if (N == 3) {
				newCellSet.add(newCell);
			} else if (N == 2 && cellSet.contains(newCell)) {
				newCellSet.add(newCell);
			}
		}
		cellSet = newCellSet;
		calculateNeighbors();
	}

	/*
	 * The key is the set of positions which has at least one neighbour.
	 * The value is how many cells it is surrounded by.
	 */
	private void calculateNeighbors() {
		Map<Cell, Integer> result = new HashMap<>();
		for (Cell cell : cellSet) {
			int i = cell.getX();
			int j = cell.getY();
			Cell north = new Cell(i - 1, j);
			Cell northeast = new Cell(i - 1, j + 1);
			Cell east = new Cell(i, j + 1);
			Cell southeast = new Cell(i + 1, j + 1);
			Cell south = new Cell(i + 1, j);
			Cell southwest = new Cell(i + 1, j - 1);
			Cell west = new Cell(i, j - 1);
			Cell northwest = new Cell(i - 1, j - 1);
			result.put(north, 1 + result.getOrDefault(north, 0));
			result.put(northeast, 1 + result.getOrDefault(northeast, 0));
			result.put(east, 1 + result.getOrDefault(east, 0));
			result.put(southeast, 1 + result.getOrDefault(southeast, 0));
			result.put(south, 1 + result.getOrDefault(south, 0));
			result.put(southwest, 1 + result.getOrDefault(southwest, 0));
			result.put(west, 1 + result.getOrDefault(west, 0));
			result.put(northwest, 1 + result.getOrDefault(northwest, 0));
		}
		neighbors = result;
	}

	/*
	 * Print as an array of .'s and @'s.
	 */
	@Override
	public String toString() {
		// initialise array of dead cells.
		StringBuilder sb = new StringBuilder();
		for (int k = 0; k < height; k++) {
			sb.append(".".repeat(width));
			if (k < height - 1) {
				sb.append("\n");
			}
		}
		// populate with alive cells.
		for (Cell cell : cellSet) {
			sb.setCharAt((width + 1) * cell.getX() + cell.getY(), '@');
		}
		return sb.toString();
	}

}
