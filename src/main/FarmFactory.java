package main;
/*
 * Creates farms.
 */
public class FarmFactory {
	/*
	 * The farm builder works as follows:
	 * new Farm.Builder(int height, int width)
	 * .add(String nameOfPattern, int down, int right) 
	 * .add(...)
	 * .build();
	 * 
	 * Note the top left is (0,0).
	 */
	public Farm createFarm() {
		return new Farm.Builder(100,  100)
				.add("crystallizationanddecayoscillator", 0, 0)
				.build();
	}
}
