package main;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

/*
 * This is the standard code
 * for java animations.
 */

@SuppressWarnings("serial")
public class Window extends JFrame {
	String drawString = "test";

	public Window() {
		setTitle("Game of Life");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setSize(640, 640);
		setLocationRelativeTo(null);
		createBufferStrategy(2);
		launch();
	}

	public void draw() {
		Graphics2D g = (Graphics2D) getBufferStrategy().getDrawGraphics();
		g.clearRect(0, 0, getWidth(), getHeight());
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 6));
		int x0 = 20, y0 = 40;
		int dx = 6, dy = 6;
		int x = 0, y = 0;
		for (char c : drawString.toCharArray()) {
			if (c == '\n') {
				x = 0;
				y++;
			} else {
				g.drawString("" + c, x0 + x * dx, y0 + y * dy);
				x++;
			}
		}

		g.dispose();
		getBufferStrategy().show();
	}

	public void launch() {
		FarmFactory farmFactory = new FarmFactory();
		Farm farm = farmFactory.createFarm();
		final int DELAY_TIME = 20;
		Timer timer = new Timer(DELAY_TIME, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				drawString = farm.toString();
				farm.step();
				draw();
			}
		});
		timer.start();
	}


}
