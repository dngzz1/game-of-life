package main;
import java.awt.EventQueue;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			var window = new Window();
			window.setVisible(true);
		});
	}

}
