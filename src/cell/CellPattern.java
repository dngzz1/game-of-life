package cell;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game_utils.TextFileHandler;

public class CellPattern {
	public static final String URL_TEXT_ROOT = "https://www.conwaylife.com/patterns/";
	public static final String DATA_FOLDER_PATH = "data/";
	private String id;
	private String plainTextPattern = "";
	private String rle;
	private String name = ""; // name of pattern
	private String author = ""; // author of pattern
	private String rlePattern = ""; // actual pattern
	private int width;
	private int height;
	private String rule;
	private Set<Cell> cellSet;

	/*
	 * Constructor.
	 */
	public CellPattern(String id) {
		this.id = id;
		// by default, load RLE from file. Else look online.
		try {
			rle = load(id, ".rle");
		} catch (IOException e) {
			System.out.println("Retrieving " + id + ".rle online...");
			rle = loadOnline(id, ".rle");
		}
		decodeRLE(rle);
		cellSet = convertRleToSet(rlePattern);
	}

	/*
	 * Load RLE or plain text from file. 
	 * If file not found, download from HTML.
	 * This will then be saved on your hard drive for easy access.
	 */
	private static String load(String id, String extension) throws IOException {
		String fileData = TextFileHandler.readTextThrowingError(DATA_FOLDER_PATH + id + extension);
		return fileData;
	}
	private static String loadOnline(String id, String extension) {
		System.out.println("Loading " + id + " online...");
		String fileData = TextFileHandler.getHtmlFrom(URL_TEXT_ROOT + id + extension);
		TextFileHandler.writeText(fileData, DATA_FOLDER_PATH + id + extension);
		return fileData;
	}


	/*
	 * Decodes RLE. https://www.conwaylife.com/wiki/Run_Length_Encoded This function
	 * reads the html content of the string. If line starts with #C, it is a
	 * comment, and store in rleC.
	 */
	public void decodeRLE(String rle) {
		boolean preambleFinished = false;
		List<String> rleLines = Arrays.asList(rle.split("\n"));
		for (String line : rleLines) {
			if (preambleFinished) {
				rlePattern += line;
			} else if (line.startsWith("#N")) {
				name += line.replaceAll("#N ", "");
			} else if (line.startsWith("#O")) {
				author += line.replaceAll("#O ", "");
			} else if (line.startsWith("x")) {
				Matcher integerMatcher = Pattern.compile("\\d+").matcher(line);
				integerMatcher.find();
				width = Integer.valueOf(integerMatcher.group());
				integerMatcher.find();
				height = Integer.valueOf(integerMatcher.group());
				Matcher ruleMatcher = Pattern.compile("rule[ \t]+=[ \\t]+.+$").matcher(line);
				ruleMatcher.find();
				rule = ruleMatcher.group().replaceAll("rule[ \\t]+=[ \\t]+", "");
				preambleFinished = true;
			}
		}
	}

	/*
	 * Decodes plain text. https://www.conwaylife.com/wiki/Plaintext We won't really
	 * use this. The way this store strings is not very memory efficient for large
	 * cell patterns.
	 */
	public void decodePlainText(String plainText) {
		List<String> plainTextLines = Arrays.asList(plainText.split("\n"));
		for (String line : plainTextLines) {
			if (!line.startsWith("!")) {
				plainTextPattern += line + "\n";
			}
		}
		/*
		 * Now analyze plain text to get x, y etc. Height is the number of \n's. Width
		 * is the maximum length of all the lines.
		 */
		String[] plainTextPSplit = plainTextPattern.split("\n");
		height = plainTextPSplit.length;
		width = 0;
		for (int i = 0; i < plainTextPSplit.length; i++) {
			if (plainTextPSplit[i].length() > width) {
				width = plainTextPSplit[i].length();
			}
		}
	}

	/*
	 * Translating RLE format into Hash Set.
	 */
	public static Set<Cell> convertRleToSet(String iRle) {
		Set<Cell> iCellSet = new HashSet<>();
		int runCount = 0;
		String tag = "";
		Pattern rct = Pattern.compile("(\\d*)([bo\\$])");
		Matcher matcher = rct.matcher(iRle);
		int i = 0;
		int j = 0;
		while (matcher.find()) {
			if (matcher.group(1).equals("")) {
				runCount = 1;
			} else {
				runCount = Integer.valueOf(matcher.group(1));
			}
			tag = matcher.group(2);
			if (tag.equals("$")) {
				i += runCount;
				j = 0;
			} else if (tag.equals("b")) {
				j += runCount;
			} else if (tag.equals("o")) {
				for (int k = 0; k < runCount; k++) {
					iCellSet.add(new Cell(i, j));
					j++;
				}
			}
		}
		return iCellSet;
	}

//	/*
//	 * Print from set data. Mostly for debug mode.
//	 */
//	public void printPattern() {
//		for (int i = 0; i < height; i++) {
//			for (int j = 0; j < width; j++) {
//				Cell here = new Cell(i, j);
//				if (cellSet.contains(here)) {
//					System.out.print("@");
//				} else {
//					System.out.print(".");
//				}
//			}
//			System.out.print("\n");
//		}
//	}

	/*
	 * Getters.
	 */
	public String getId() {
		return id;
	}
	
	public Set<Cell> getCellSet() {
		return cellSet;
	}

	public String getRle() {
		return rle;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public String getRlePattern() {
		return rlePattern;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getRule() {
		return rule;
	}
	
	/*
	 * Override toString.
	 */
	@Override
	public String toString() {
		// initialize array of dead cells.
		StringBuilder sb = new StringBuilder();
		for (int k = 0; k < height; k++) {
			sb.append(".".repeat(width));
			if (k < height - 1) {
				sb.append("\n");
			}
		}
		
		// populate with alive cells.
		for (Cell cell : cellSet) {
			sb.setCharAt((width + 1) * cell.getX() + cell.getY(), '@');
		}
		
		return sb.toString();
	}

}
